topo = {
    name: 'portal2',
    nodes: [
        deb('px', 2, 2),  // proxy
        deb('ma0', 2, 4),  // master
        deb('ma1', 2, 4),  // master
        deb('ma2', 2, 4),  // master
        deb('sw', 8, 8),  // service-worker
        deb('xw', 8, 8),  // xdc-worker
        deb('ops', 2, 2), // operations
        stor('st0'),   // storage
        stor('st1'),   // storage
        stor('st2'),   // storage
    ],
    switches: [
        cx('psw'),
    ],
    links: [
        Link('px', 1, 'psw', 1),
        Link('ma0', 1, 'psw', 2),
        Link('ma1', 1, 'psw', 3),
        Link('ma2', 1, 'psw', 4),
        Link('sw', 1, 'psw', 5),
        Link('xw', 1, 'psw', 6),
        Link('ops', 1, 'psw', 7),
        Link('st0', 1, 'psw', 8),
        Link('st1', 1, 'psw', 9),
        Link('st2', 1, 'psw', 10),
    ],
}

function deb(name, cores, mem) {
    return {
        name: name,
        image: 'debian-bullseye',
        proc: { cores: cores },
        memory: { capacity: GB(mem) },
    }
}

function stor(name) {
    return {
        name: name,
        image: 'debian-bullseye',
        proc: { cores: 4 },
        memory: { capacity: GB(4) },
        disks: [
            { size: "5G", dev: 'vdb', bus: 'virtio' },
            { size: "5G", dev: 'vdc', bus: 'virtio' },
            { size: "5G", dev: 'vdd', bus: 'virtio' }
        ],
    }
}

function cx(name) {
  return {
    name: name,
    image: 'cumulusvx-4.1',
    cpu: { cores: 2 },
    memory: { capacity: GB(2) }
  };
}
