#!/bin/bash

ansible-playbook                    \
    -i .rvn/ansible-hosts           \
    -i ansible-interpreters         \
    configure-switch.yml

ansible-playbook                    \
    -i .rvn/ansible-hosts           \
    -i ansible-interpreters         \
    configure-net.yml

ansible-playbook                    \
    -i .rvn/ansible-hosts           \
    -i ansible-interpreters         \
    --extra-vars @portal-deploy.yml \
    ../portal-deploy/storage.yml

ansible-playbook                    \
    -i .rvn/ansible-hosts           \
    -i ansible-interpreters         \
    --extra-vars @portal-deploy.yml \
    ../portal-deploy/provision.yml

ansible-playbook                    \
    -i .rvn/ansible-hosts           \
    -i ansible-interpreters         \
    configure-rvn.yml

ansible-playbook                    \
    -i .rvn/ansible-hosts           \
    -i ansible-interpreters         \
    --extra-vars @portal-deploy.yml \
    ../portal-deploy/csi.yml

ansible-playbook                    \
    -i .rvn/ansible-hosts           \
    -i ansible-interpreters         \
    --extra-vars @portal-deploy.yml \
    ../portal-deploy/merge/merge.yml
