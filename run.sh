#!/bin/bash

RED="\e[31m"
BLUE="\e[34m"
GREEN="\e[32m"
CYAN="\e[36m"
NORMAL="\e[39m"
BLINK="\e[5m"
RESET="\e[0m"

blue() { 
  echo -e "$BLUE$1$NORMAL" 
}
green() { 
  echo -e "$GREEN$1$NORMAL" 
}
cyan() { 
  echo -e "$CYAN$1$NORMAL" 
}
stage() {
  echo -e "$BLUE$1$BLINK 🔨$RESET"
}

stage "destroying any exiting topologies"
rvn destroy
rm -rf /tmp/portal-keygen

stage "building topology"
rvn build

stage "deploying topology"
rvn deploy

stage "waiting for topology to come up"
rvn pingwait ma0 ma1 ma2 sw xw ops px psw st0 st1 st2

stage "configuring topology"
rvn configure
rvn status

stage "deploying k8s"
./setup.sh
