#!/bin/bash

if [[ $# -ne 1 ]]; then
  echo "usage: local-merge-setup.sh <username>"
  return 1
fi

username=$1

# run me as source to get a workable mergetb cli setup

# setup mergetb cli
shopt -s expand_aliases
alias mergetb="mergetb --cert /tmp/portal-keygen/ca.pem"

# set workstation routes to virtual merge portal
if ip route | grep -q 2.3.4; then
  sudo ip route del 2.3.4.0/24
fi

px=$(sudo rvn ip px)
sudo ip route add 2.3.4.0/24 via $px

# sign-in to merge
if [[ ! -f ~/.merge/token ]]; then
  if [[ $passwd ]]; then
    mergetb login $username --passwd $passwd
  else
    mergetb login $username
  fi
  if [[ $? -ne 0 ]]; then
      return
  fi
fi

mergetb join
if [[ $? -ne 0 ]]; then
    return
fi

sudo ansible-playbook -i .rvn/ansible-hosts --extra-vars "user=$username" activate.yml
